package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

type Record struct {
  curTime string
  URL string
  HTTPResponseCode int
}

var (
  timeStamp = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "the_timestamp_for_the_last_request",
    Help: "The timestamp of the last request",
  })
  URLRequest = prometheus.NewGaugeVec(prometheus.GaugeOpts{
    Name: "the_last_requested_url",
    Help: "The last requested url",
  },
  []string {
    "url",
  },
  )
  ResponseHTTPCode = prometheus.NewGauge(prometheus.GaugeOpts{
    Name: "the_last_http_code",
    Help: "The last received HTPP code",
  })
  )

func main()  {
  argLength := len(os.Args[1:])
  pushGW := os.Getenv("PGWURL")
  fmt.Printf("Arg length is %d\n", argLength)
  tr := &http.Transport{
    IdleConnTimeout: 5 * time.Second,
  }
  client := &http.Client{
    Transport: tr,
  }
  for _, url := range os.Args[1:] {
    registry := prometheus.NewRegistry()
    registry.MustRegister(timeStamp, URLRequest, ResponseHTTPCode)
    pusher := push.New(pushGW, "requests_results").Gatherer(registry)
    fmt.Println(url)
    requestURL := fmt.Sprint(url)
    res, err := client.Head(requestURL)
    fmt.Println(res.StatusCode)
    fmt.Println(err)
    record := Record{
      curTime: time.Now().Format("01-02-2006 15:04:05 Mon"),
      URL: requestURL,
      HTTPResponseCode: res.StatusCode,
    }
    fmt.Println(record)
    timeStamp.SetToCurrentTime()
    URLRequest.With(prometheus.Labels{"url": requestURL}).Inc()
    ResponseHTTPCode.Set(float64(res.StatusCode))
    if err != nil {
      fmt.Println("Request failed")
    }
    if err:= pusher.Add(); err != nil {
      fmt.Println("Could not push to PGW")
    }
  }
}
