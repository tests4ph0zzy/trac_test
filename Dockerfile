FROM golang:1.18-alpine
ADD . /go/src/trac_test
RUN apk add --update --no-cache git
RUN go get github.com/prometheus/client_golang/prometheus@latest && \
    go get github.com/prometheus/client_golang/prometheus/push@latest && \
    go install trac_test

FROM alpine:latest
COPY --from=0 /go/bin/trac_test .
ENV PGWURL http://127.0.0.1:9091
RUN env
ENTRYPOINT ["./trac_test"]
